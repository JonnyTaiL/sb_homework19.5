﻿// SB_HomeWork19.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;

class Animal
{
private:
    string Sound;
public:
   virtual void Voice()
   {
       cout << "*Animal Sound*" << endl;
   }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof" << endl;
    }
};


class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow" << endl;
    }
};


class Fish : public Animal
{
public:
    void Voice() override
    {
        cout << "Good Day Sir" << endl;
    }
};




int main()

{
    int size = 3;
    Animal* pa[3];


    pa[0] = &Dog();
    pa[1] = &Cat();
    pa[2] = &Fish();


    for (int i = 0; i < size; i++)
    {
        pa[i]->Voice();
    }
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
